require "rspec"
require "gitlab_exporter"

require "open3"
require "tmpdir"

$LOAD_PATH.unshift File.expand_path(".")
Dir["spec/support/**/*.rb"].each do |f| require f end

RSpec.configure do |config|
  config.filter_run_excluding integration: true
end

class GitRepoBuilder
  def origin
    @origin ||= create_origin
  end

  def cloned_repo
    @cloned_repo ||= clone_origin
  end

  def cleanup
    FileUtils.rm_r(@origin) if @origin
    FileUtils.rm_r(@cloned_repo) if @cloned_repo
  end

  private

  def create_origin
    path = Dir.mktmpdir
    Open3.capture3("git init", chdir: path)
    Open3.capture3("git commit --allow-empty -m 'Beep'", chdir: path)
    Open3.capture3("git checkout -b other", chdir: path)
    path
  end

  def clone_origin
    path = Dir.mktmpdir
    Dir.rmdir(path)
    Open3.capture3("git clone #{origin} #{path}")
    Open3.capture3("git checkout master", chdir: path)
    path
  end
end

class CLIArgs
  def initialize(args, options: {})
    @arguments = args
    @options = options
  end

  def options
    yield self

    self
  end

  def on(flag, *_)
    match = @options.find { |regex, _| regex.match?(flag) }

    yield match[1] if match
  end

  def banner=(banner); end

  def parse!
    @arguments
  end

  def shift
    @arguments.shift
  end
end
